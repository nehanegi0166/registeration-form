import React,{useState, useEffect} from 'react';
import Validation from './Validation';

const SignupForm = ({submitForm}) => {
    
    //Keep track of values
    const [values, setValues] = useState(
        {
            fullname:"",
            email:"",
            password:"",
            description:"",
            gender:""
        }
    );

    const [errors, setErrors] = useState({});
    
    const [dataIsCorrect, setDataIsCorrect] = useState(false);

    const toppings = [
        {
          name: "Software Development",
        },
        {
          name: "Mechanical Engineering",
        },
        {
          name: "Chemical Engineering",
        },
        {
          name: "Technical",
        },
        {
          name: "Non-technical",
        }
      ];

    const handleChange = (event) =>{
        //Set value for each input field
        setValues ({
            ...values,
            [event.target.name]: event.target.value,
        });
    };

    const handleFormSubmit = (event) =>{
        event.preventDefault();
        setErrors(Validation(values));
        setDataIsCorrect(true);
    };

    useEffect(() => {
        if(Object.keys(errors).length === 0 && dataIsCorrect){
            submitForm(true);
        }
    },[errors]);
    return (
        <div className='container'>
            <div className='app-wrapper'>
                <div>
                    <h2 className='title'>Create Account</h2>
                </div>
                <form className='form-wrapper'>
                    <div className='name'>
                        <label className='label'>Full Name</label>
                        <input className='input' type="text" name='fullname' value={values.fullname} onChange={(handleChange)} />
                        {errors.fullname && <p className='error'>{errors.fullname}</p>}
                    </div>
                    <div className='email'>
                        <label className='label'>Email</label>
                        <input className='input' type="email" name='email' value={values.email} onChange={handleChange}/>
                        {errors.email && <p className='error'>{errors.email}</p>}
                    </div>
                    <div className='password'>
                        <label className='label'>Password</label>
                        <input className='input' type="password" name='password' value={values.password} onChange={handleChange}/>
                        {errors.password && <p className='error'>{errors.password}</p>}
                    </div>
                    <div className='description'>
                        <textarea name="description" placeholder='Description' value={values.description} onChange={handleChange} />
                        {errors.description && <p className='error'>{errors.description}</p>}    
                    </div>
                    <div className='gender'>
                        <label>Gender</label><br />
                        <select name="gender" value={values.gender} onChange={handleChange}>
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                            <option value="transgender">Transgender</option>
                        </select>
                        {errors.gender && <p className='error'>{errors.gender}</p>}    
                    </div>
                    <div className="checkbox">
                        <label>Area of Interest</label><br />
                        <ul className="toppings-list">
                            {toppings.map(({ name }, index) => {
                            return (
                                <li key={index}>
                                <div className="toppings-list-item">
                                    <div className="left-section">
                                    <input
                                        type="checkbox"
                                        id={`custom-checkbox-${index}`}
                                        name={name}
                                        value={name}
                                    />
                                    <label className='checkval' htmlFor={`custom-checkbox-${index}`}>{name}</label>
                                    </div>
                                </div>
                                </li>
                            );
                            })}
                            <li>
                            </li>
                        </ul>
                    </div>
                    <div className='file-upload'>
                    <label>Upload file:</label><br />
                        <input type="file"/>
                    </div>
                    <div>
                        <button className='submit' onClick={handleFormSubmit}>Sign Up</button>
                    </div>
                </form>
            </div>
        </div>
    );
};

export default SignupForm
